﻿namespace Data
{
	public class BaseArticleImage
	{
		public string Url { get; set; }

		public string Format { get; set; }

		public int Height { get; set; }

		public int Width { get; set; }

		public string Type { get; set; }

		public string Subtype { get; set; }

		public string Caption { get; set; }

		public string Copyright { get; set; }
	}
}
