﻿using System.Collections.Generic;

namespace Data
{
	public class BaseArticleModel
	{
		public int Num_Results { get; set; }

		public IList<BaseArticleResults> Results { get; set; }
	}
}
