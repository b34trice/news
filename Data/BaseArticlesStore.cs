﻿namespace Data
{
	public class BaseArticlesStore
	{
		private static BaseArticlesStore instance;

		public BaseArticleModel baseArticle = new BaseArticleModel();

		public static BaseArticlesStore GetInstance()
		{
			if (instance == null)
			{
				instance = new BaseArticlesStore();
				return instance;
			}

			return instance;
		}
	}
}
