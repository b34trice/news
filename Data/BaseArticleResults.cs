﻿using System;
using System.Collections.Generic;

namespace Data
{
	public class BaseArticleResults
	{
		public string Section { get; set; }

		public string Subsection { get; set; }

		public string Title { get; set; }

		public string Abstract { get; set; }

		public string Url { get; set; }

		public string Byline { get; set; }

		public DateTime First_published_date { get; set; }

		public IList<BaseArticleImage> Multimedia { get; set; }

	}
}
