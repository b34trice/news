﻿using System;
using System.Windows.Input;

namespace News.Command
{
	public class RelayCommand : ICommand
	{
		public event EventHandler CanExecuteChanged;

		private readonly Action<object> _execute;
		private readonly bool _canExecute;

		public RelayCommand(Action<object> execute, bool canExecute = true)
		{
			_execute = execute;
			_canExecute = canExecute;
		}

		public void Execute(object parameter)
		{
			_execute(parameter);
		}

		public bool CanExecute(object parameter)
		{
			return _canExecute;
		}

		public void OnCanExecuteChanged()
		{
			CanExecuteChanged?.Invoke(this, new EventArgs());
		}
	}
}