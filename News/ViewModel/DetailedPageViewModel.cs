﻿using Services;
using Services.Interfaces;
using Services.Model;
using Windows.UI.Xaml;

namespace News.ViewModel
{
	public class DetailedPageViewModel : BaseViewModel
	{
		public Style AppStyle
		{
			get => _appStyle;
			set
			{
				_appStyle = value;
				OnPropertyChanged(nameof(_appStyle));
			}
		}

		public Article Article { get; set; }

		private Style _appStyle;

		private readonly ILoggerService _loggerService;
		private readonly IAppSettingsService _appSettingsService;
		private readonly IJsonParserService<Article> _jsonParserService;

		public DetailedPageViewModel(ILoggerService loggerService, IAppSettingsService appSettingsService, IStyleGenerationService styleGenerationService, IJsonParserService<Article> jsonParserService)
		{
			_loggerService = loggerService;
			_appSettingsService = appSettingsService;
			_jsonParserService = jsonParserService;
			AppStyle = styleGenerationService.GetStyle(_appSettingsService.GetSettings());

			_loggerService.LogInfo(LoggerMessages.LoggerLoadDetailedPageMessage);
		}

		public void InitArticle(string json)
		{
			Article = _jsonParserService.GetObjectFromJson(json);
		}
	}
}