﻿using News.View;
using Services;
using Services.Interfaces;
using Services.Model;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace News.ViewModel
{
	public class MainPageViewModel : BaseViewModel
	{
		public Style AppStyle
		{
			get => _appStyle;
			set
			{
				_appStyle = value;
				OnPropertyChanged(nameof(_appStyle));
			}
		}

		private Style _appStyle;

		private readonly INavigationService _navigationService;
		private readonly ILoggerService _loggerService;
		private readonly IAppSettingsService _appSettingsService;

		public MainPageViewModel(ILoggerService loggerService, IAppSettingsService appSettingsService, IStyleGenerationService styleGenerationService, INavigationService navigationService)
		{
			_loggerService = loggerService;
			_loggerService.Config();
			_navigationService = navigationService;
			_appSettingsService = appSettingsService;
			AppStyle = styleGenerationService.GetStyle(_appSettingsService.GetSettings());

			_loggerService.LogInfo(LoggerMessages.LoggerLoadMainPageMessage);
		}

		public void InitPages(Frame frame)
		{
			_navigationService.SetFrame(frame);
			_navigationService.SetPageConfig(Pages.DetailedPageView, typeof(DetailedPageView));
			_navigationService.SetPageConfig(Pages.FeedPageView, typeof(FeedPageView));
			_navigationService.SetPageConfig(Pages.MainPageView, typeof(MainPageView));
			_navigationService.SetPageConfig(Pages.SettingsPageView, typeof(SettingsPageView));
		}

		public void GoToSettings()
		{
			_navigationService.NavigateTo(Pages.SettingsPageView);
		}

		public void GoToFeed()
		{
			_navigationService.NavigateTo(Pages.FeedPageView);
		}

		public void GoToDetailed()
		{
			_navigationService.NavigateTo(Pages.DetailedPageView);
		}

		public void GoBack()
		{
			_navigationService.GoBack();
		}
	}
}