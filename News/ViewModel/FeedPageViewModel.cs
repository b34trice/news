﻿using Services;
using Services.Interfaces;
using Services.Model;
using System.Collections.ObjectModel;
using Windows.UI.Xaml;

namespace News.ViewModel
{
	public class FeedPageViewModel : BaseViewModel
	{
		public Style AppStyle
		{
			get => _appStyle;
			set
			{
				_appStyle = value;
				OnPropertyChanged(nameof(_appStyle));
			}
		}

		public Article SelectedArticle { get; set; }

		public AppSettings AppSettings { get; set; }

		public ObservableCollection<Article> Articles
		{
			get => ArticlesSingleton.GetInstance().Articles;
			set => ArticlesSingleton.GetInstance().Articles = value;
		}

		private Style _appStyle;

		private readonly INavigationService _navigationService;
		private readonly IApiService _apiService;
		private readonly ILoggerService _loggerService;
		private readonly IAppSettingsService _appSettingsService;
		private readonly ICachingService _cachingService;
		private readonly IJsonParserService<Article> _jsonParserService;


		public FeedPageViewModel(IApiService apiService, ILoggerService loggerService, IAppSettingsService appSettingsService,
			IStyleGenerationService styleGenerationService, INavigationService navigationService, ICachingService cachingService, IJsonParserService<Article> jsonParserService)
		{
			_apiService = apiService;
			_loggerService = loggerService;
			_navigationService = navigationService;
			_appSettingsService = appSettingsService;
			_cachingService = cachingService;
			_jsonParserService = jsonParserService;

			AppSettings = _appSettingsService.GetSettings();
			AppStyle = styleGenerationService.GetStyle(AppSettings);

			_loggerService.LogInfo(LoggerMessages.LoggerLoadPageMessage);
		}

		public void GoToDetailed()
		{
			_navigationService.NavigateTo(Pages.DetailedPageView, _jsonParserService.GetJsonFromObject(SelectedArticle));
		}

		public void LoadingArticles()
		{
			_loggerService.LogInfo(LoggerMessages.LoggerStartRefreshArticleMessage);
			ObservableCollection<Article> newArticles = _apiService.GetArticles(Articles.Count);

			foreach (Article article in newArticles)
			{
				Articles.Add(article);
			}
			_loggerService.LogInfo(LoggerMessages.LoggerEndRefreshArticlesMessage);
			OnPropertyChanged(nameof(Articles));
		}

		public void InitArticles()
		{
			Articles = _cachingService.GetCache();
			if (Articles == null)
			{
				LoadArticlesFromApi();
			}
		}

		private void LoadArticlesFromApi()
		{
			_loggerService.LogInfo(LoggerMessages.LoggerBeginGetDataFromApiMessage);
			Articles = _apiService.GetArticles(0);
			_loggerService.LogInfo(LoggerMessages.LoggerEndGetDataFromApiMessage);
		}
	}
}