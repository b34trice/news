﻿using News.Command;
using Services.Interfaces;
using Services.Model;
using System.Collections.Generic;
using Windows.UI.Xaml;

namespace News.ViewModel
{
	public class SettingsViewModel : BaseViewModel
	{
		public Style AppStyle
		{
			get => _appStyle;
			set
			{
				_appStyle = value;
				OnPropertyChanged(nameof(AppStyle));
			}
		}

		public int FontSize { get; set; }

		public Theme SelectedTheme { get; set; }

		public RelayCommand ApplySettings => _applySettings;

		public IList<Theme> Themes;

		private AppSettings _appSettings;
		private RelayCommand _applySettings;
		private Style _appStyle;

		private readonly ILoggerService _loggerService;
		private readonly IAppSettingsService _appSettingsService;
		private readonly IStyleGenerationService _styleGenerationService;

		private const string _themeDark = "Dark";
		private const string _themeLight = "Light";

		public SettingsViewModel(ILoggerService loggerService, IAppSettingsService appSettingsService, IStyleGenerationService styleGenerationService)
		{
			_loggerService = loggerService;
			_styleGenerationService = styleGenerationService;
			_appSettingsService = appSettingsService;

			InitUi();
			InitCommands();
			InitThemes();
			InitSelectedTheme();

			_loggerService.LogInfo(LoggerMessages.LoggerLoadSettingsPageMessage);
		}

		private void InitUi()
		{
			_appSettings = _appSettingsService.GetSettings();
			FontSize = _appSettings.FontSize;
			AppStyle = _styleGenerationService.GetStyle(_appSettings);
		}

		private void InitThemes()
		{
			Themes = new List<Theme>
			{
				new Theme(_themeDark),
				new Theme(_themeLight)
			};
		}

		private void InitSelectedTheme()
		{
			if (_appSettings.ApplicationTheme == ElementTheme.Dark)
			{
				SelectedTheme = Themes[0];
			}
			else
			{
				SelectedTheme = Themes[1];
			}
		}

		private void InitCommands()
		{
			_applySettings = new RelayCommand(
				(parametr) =>
				{
					ElementTheme theme;

					if (SelectedTheme.ThemeName.Equals(_themeDark))
					{
						theme = ElementTheme.Dark;
					}
					else
					{
						theme = ElementTheme.Light;
					}

					_appSettings = new AppSettings(FontSize, theme);
					_appSettingsService.SetSettings(_appSettings);
					InitUi();
				}
				);
		}
	}
}