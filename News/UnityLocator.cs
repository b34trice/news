﻿using Data;
using Services;
using Services.Interfaces;
using Services.Model;
using Unity;

namespace News.ViewModel
{
	public class UnityLocator
	{
		public MainPageViewModel MainPageViewModel => _unityContainer.Resolve<MainPageViewModel>();
		public SettingsViewModel SettingsViewModel => _unityContainer.Resolve<SettingsViewModel>();
		public FeedPageViewModel FeedPageViewModel => _unityContainer.Resolve<FeedPageViewModel>();
		public DetailedPageViewModel DetailedPageViewModel => _unityContainer.Resolve<DetailedPageViewModel>();

		private static readonly UnityContainer _unityContainer;

		static UnityLocator()
		{
			_unityContainer = new UnityContainer();
			InitServices();
			InitViewModels();

		}

		private static void InitViewModels()
		{
			_unityContainer.RegisterSingleton<MainPageViewModel>();
			_unityContainer.RegisterSingleton<FeedPageViewModel>();
			_unityContainer.RegisterSingleton<SettingsViewModel>();
			_unityContainer.RegisterSingleton<DetailedPageViewModel>();
		}

		private static void InitServices()
		{
			_unityContainer.RegisterSingleton<ILoggerService, LoggerService>();
			_unityContainer.RegisterSingleton<INavigationService, NavigationService>();
			_unityContainer.RegisterSingleton<IAppSettingsService, AppSettingsService>();
			_unityContainer.RegisterSingleton<IStyleGenerationService, StyleGenerationService>();
			_unityContainer.RegisterSingleton<IApiService, ApiService>();
			_unityContainer.RegisterSingleton<IMapperService, MapperService>();
			_unityContainer.RegisterSingleton<ICachingService, CachingService>();
			_unityContainer.RegisterSingleton<IFileManagerService, FileManagerService>();

			_unityContainer.RegisterSingleton<IJsonParserService<Article>, JsonParserService<Article>>();
			_unityContainer.RegisterSingleton<IJsonParserService<BaseArticlesStore>, JsonParserService<BaseArticlesStore>>();
			_unityContainer.RegisterSingleton<IJsonParserService<BaseArticleModel>, JsonParserService<BaseArticleModel>>();
			_unityContainer.RegisterSingleton<IJsonParserService<AppSettings>, JsonParserService<AppSettings>>();
		}
	}
}