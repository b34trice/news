﻿using News.ViewModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace News.View
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public sealed partial class FeedPageView : Page
	{
		public FeedPageViewModel ViewModel { get; set; }

		public FeedPageView()
		{
			InitializeComponent();
			DataContextChanged += OnDataContextChanged;
		}

		private void OnFeedListSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			ViewModel.GoToDetailed();
		}

		private void OnDataContextChanged(FrameworkElement s, DataContextChangedEventArgs e)
		{
			ViewModel = DataContext as FeedPageViewModel;
			ViewModel.InitArticles();
		}

		private void OnScrollViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
		{
			if (Scroll.VerticalOffset == Scroll.ScrollableHeight)
			{
				ViewModel.LoadingArticles();
			}
		}

		private void OnPageLoading(FrameworkElement sender, object args)
		{
			Style baseStyle = Resources["TextBlockBaseStyle"] as Style;
			baseStyle.BasedOn = ViewModel.AppStyle;
		}
	}
}