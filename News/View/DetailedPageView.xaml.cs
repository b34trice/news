﻿using News.ViewModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace News.View
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public sealed partial class DetailedPageView : Page
	{
		public DetailedPageViewModel ViewModel { get; set; }

		private string jsonArticle;

		public DetailedPageView()
		{
			InitializeComponent();
			DataContextChanged += (s, e) => { ViewModel = DataContext as DetailedPageViewModel; ViewModel.InitArticle(jsonArticle); };
		}

		protected override void OnNavigatedTo(NavigationEventArgs e)
		{
			jsonArticle = e.Parameter as string;
		}

	}
}