﻿using News.ViewModel;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace News.View
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public sealed partial class SettingsPageView : Page
	{
		public SettingsViewModel ViewModel { get; set; }

		public SettingsPageView()
		{
			InitializeComponent();
			DataContextChanged += (s, e) => { ViewModel = DataContext as SettingsViewModel; };
		}
	}
}