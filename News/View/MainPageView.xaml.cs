﻿using News.ViewModel;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace News.View
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public sealed partial class MainPageView : Page
	{
		public MainPageViewModel ViewModel { get; set; }

		public MainPageView()
		{
			InitializeComponent();
			DataContextChanged += (s, e) =>
			{
				ViewModel = DataContext as MainPageViewModel;
				ViewModel.InitPages(MainFrame);
			};
		}

		private void OnNavigationViewSelectionChanged(NavigationView sender, NavigationViewSelectionChangedEventArgs args)
		{
			NavigationView.IsBackEnabled = true;
			if (args.IsSettingsSelected)
			{
				ViewModel.GoToSettings();
			}
			else if (args.SelectedItemContainer != null)
			{
				ViewModel.GoToFeed();
			}
		}

		private void OnNavigationViewLoaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
		{
			NavigationView.IsPaneOpen = false;
			NavigationView.SelectedItem = NavigationView.MenuItems[0];
		}

		private void OnNavigationViewBackRequested(NavigationView sender, NavigationViewBackRequestedEventArgs args)
		{
			ViewModel.GoBack();
		}
	}
}