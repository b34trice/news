﻿using Windows.UI.Xaml;

namespace Services.Model
{
	public class AppSettings
	{
		public int FontSize { get; set; }

		public ElementTheme ApplicationTheme { get; set; }

		public AppSettings(int fontSize, ElementTheme theme)
		{
			FontSize = fontSize;
			ApplicationTheme = theme;
		}
	}
}