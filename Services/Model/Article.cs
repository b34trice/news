﻿using Services.Model;
using System;
using System.ComponentModel;

namespace Services
{
	public class Article : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		private ArticleImage _articleImage;
		private DateTime _publishedDate;
		private string _url;
		private string _context;
		private string _title;
		private string _abstract;
		private string _byline;

		public ArticleImage ArticleImage
		{
			get => _articleImage;
			set
			{
				_articleImage = value;
				OnPropertyChanged(nameof(_articleImage));
			}
		}

		public DateTime PublishedDate
		{
			get => _publishedDate;
			set
			{
				_publishedDate = value;
				OnPropertyChanged(nameof(_publishedDate));
			}
		}

		public string Url
		{
			get => _url;
			set
			{
				_url = value;
				OnPropertyChanged(nameof(_url));
			}
		}

		public string Context
		{
			get => _context;
			set
			{
				_context = value;
				OnPropertyChanged(nameof(_context));
			}
		}

		public string Title
		{
			get => _title;
			set
			{
				_title = value;
				OnPropertyChanged(nameof(_title));
			}
		}

		public string Abstract
		{
			get => _abstract;
			set
			{
				_abstract = value;
				OnPropertyChanged(nameof(_abstract));
			}
		}

		public string Byline
		{
			get => _byline;
			set
			{
				_byline = value;
				OnPropertyChanged(nameof(_byline));
			}
		}

		public void OnPropertyChanged(string prop)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
		}
	}
}