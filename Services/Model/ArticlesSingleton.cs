﻿using System.Collections.ObjectModel;

namespace Services.Model
{
	public class ArticlesSingleton
	{
		public ObservableCollection<Article> Articles { get; set; }

		private static ArticlesSingleton _instance;

		public static ArticlesSingleton GetInstance()
		{
			if (_instance != null)
			{
				return _instance;
			}

			_instance = new ArticlesSingleton();
			return _instance;
		}
	}
}