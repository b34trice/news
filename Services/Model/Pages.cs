﻿namespace Services
{
	public static class Pages
	{
		public static readonly string DetailedPageView = "DetailedPageView";

		public static readonly string FeedPageView = "FeedPageView";

		public static readonly string MainPageView = "MainPageView";

		public static readonly string SettingsPageView = "SettingsPageView";
	}
}