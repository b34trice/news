﻿namespace Services.Model
{
	public static class LoggerMessages
	{
		//Pages
		public static readonly string LoggerLoadPageMessage = "Load feed page";
		public static readonly string LoggerLoadMainPageMessage = "Load main page";
		public static readonly string LoggerLoadDetailedPageMessage = "Load detailed page";
		public static readonly string LoggerLoadSettingsPageMessage = "Load settings page";

		//Api
		public static readonly string LoggerEndGetJsonMessage = "End get json from Api";
		public static readonly string LoggerBeginGetJsonMessage = "Begin get json from Api";
		public static readonly string LoggerEndGetDataFromApiMessage = "End get data from Api";
		public static readonly string LoggerBeginGetDataFromApiMessage = "Begin get data from Api";

		//Feed page
		public static readonly string LoggerEndRefreshArticlesMessage = "End refresh articles";
		public static readonly string LoggerStartRefreshArticleMessage = "Start refresh articles";

		//Mapper		
		public static readonly string LoggerEndMapperWorkMessage = "End mapper work";
		public static readonly string LoggerEndArticleMessage = "End article mapping";
		public static readonly string LoggerBeginMappingMessage = "Begin mapper work";
		public static readonly string LoggerEndMappingImageMessage = "End mapping image";
		public static readonly string LoggerStartMappingImageMessage = "Start mapping image";
		public static readonly string LoggerStartNewArticleMessage = "Start new article mapping";

		//Json parser
		public static readonly string LoggerEndDeserializeMessage = "End Deserialize";
		public static readonly string LoggerBeginDeserializeMessage = "Begin Deserialize";

		//Settings service
		public static readonly string LoggerGetSettingsMessage = "Start get settings";
		public static readonly string LoggerMissingFileMessage = "Settings file is missing";

		//FileManager service
		public static readonly string LoggerWriteDataMessage = "Write data to file";
		public static readonly string LoggerReadDataMessage = "Read data from file";

		//Caching service
		public static readonly string LoogerEndReadCacheMessage = "End read cache";
		public static readonly string LoggerCacheIsActualMessage = "Cache is actual";
		public static readonly string LoggerCacheIsMissingMessage = "Cache is missing";
		public static readonly string LoggerStartReadCacheMessage = "Start read cache";
		public static readonly string LoggerEndWriteCacheMessage = "End writing cache";
		public static readonly string LoggerStartWriteCacheMessage = "Start writing cache";
	}
}
