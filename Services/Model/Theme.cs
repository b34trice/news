﻿namespace Services.Model
{
	public class Theme
	{
		public string ThemeName { get; set; }

		public Theme(string themeName)
		{
			ThemeName = themeName;
		}
	}
}