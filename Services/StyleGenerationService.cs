﻿using Services.Interfaces;
using Services.Model;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace Services
{
	public class StyleGenerationService : IStyleGenerationService
	{
		public Style GetStyle(AppSettings settings)
		{
			Style style;

			if (settings.ApplicationTheme == ElementTheme.Dark)
			{
				style = GetDark();
			}
			else
			{
				style = GetLight();
			}

			style.Setters.Add(new Setter { Property = Control.FontSizeProperty, Value = settings.FontSize });
			style.Setters.Add(new Setter { Property = TextBlock.FontSizeProperty, Value = settings.FontSize });
			style.TargetType = typeof(object);
			return style;
		}

		private Style GetDark()
		{
			Style style = new Style();
			style.Setters.Add(new Setter { Property = Control.BackgroundProperty, Value = new SolidColorBrush(Colors.Black) });
			style.Setters.Add(new Setter { Property = Control.ForegroundProperty, Value = new SolidColorBrush(Colors.White) });
			style.Setters.Add(new Setter { Property = TextBlock.ForegroundProperty, Value = new SolidColorBrush(Colors.White) });
			return style;
		}

		private Style GetLight()
		{
			Style style = new Style();
			style.Setters.Add(new Setter { Property = Control.BackgroundProperty, Value = new SolidColorBrush(Colors.White) });
			style.Setters.Add(new Setter { Property = Control.ForegroundProperty, Value = new SolidColorBrush(Colors.Black) });
			style.Setters.Add(new Setter { Property = TextBlock.ForegroundProperty, Value = new SolidColorBrush(Colors.Black) });
			return style;
		}
	}
}