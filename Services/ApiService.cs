﻿using Data;
using Services.Interfaces;
using Services.Model;
using System;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;

namespace Services
{
	public class ApiService : IApiService
	{

		private readonly IMapperService _mapperService;
		private readonly ILoggerService _loggerService;
		private readonly ICachingService _cachingService;
		private readonly IJsonParserService<BaseArticleModel> _jsonParserService;

		public ApiService(IMapperService mapperService, ILoggerService loggerService, ICachingService cachingService, IJsonParserService<BaseArticleModel> jsonParserService)
		{
			_mapperService = mapperService;
			_loggerService = loggerService;
			_cachingService = cachingService;
			_jsonParserService = jsonParserService;
		}

		public ObservableCollection<Article> GetArticles(int articlesCounter)
		{
			BaseArticleModel baseArticle;

			string json = GetJson(articlesCounter);
			_loggerService.LogInfo(LoggerMessages.LoggerBeginDeserializeMessage);
			baseArticle = _jsonParserService.GetObjectFromJson(json);
			if (baseArticle != null)
			{
				SetBaseArticlesStore(baseArticle);
				_cachingService.WriteCache(BaseArticlesStore.GetInstance());
			}
			_loggerService.LogInfo(LoggerMessages.LoggerBeginMappingMessage);
			return _mapperService.GetArticles(baseArticle);
		}

		private void SetBaseArticlesStore(BaseArticleModel baseArticle)
		{
			BaseArticlesStore instanceStore = BaseArticlesStore.GetInstance();
			
			instanceStore.baseArticle.Num_Results = baseArticle.Num_Results;
			if (instanceStore.baseArticle.Results == null)
			{
				instanceStore.baseArticle.Results = baseArticle.Results;
				return;
			}
			foreach (BaseArticleResults article in baseArticle.Results)
			{
				instanceStore.baseArticle.Results.Add(article);
			}
		}

		private string GetJson(int articlesCounter)
		{
			_loggerService.LogInfo(LoggerMessages.LoggerBeginGetJsonMessage);
			Uri uri = new UriBuilderService().GetUri(articlesCounter);
			string json = GetJsonAsync(uri).Result;
			return json;
		}

		private async Task<string> GetJsonAsync(Uri uri)
		{
			using (HttpClient client = new HttpClient())
			{
				try
				{
					using (HttpResponseMessage response = await client.GetAsync(uri).ConfigureAwait(false))
					{
						if (!response.IsSuccessStatusCode)
						{
							_loggerService.LogError(new HttpRequestException().Message);
							throw new HttpRequestException();
						}

						_loggerService.LogInfo(LoggerMessages.LoggerEndGetJsonMessage);
						return await response.Content.ReadAsStringAsync().ConfigureAwait(false);
					}
				}
				catch (HttpRequestException e)
				{
					_loggerService.LogError(e.Message);
					return "";
				}
			}
		}
	}
}