﻿using Newtonsoft.Json;
using Services.Interfaces;
using Services.Model;

namespace Services
{
	public class JsonParserService<T> : IJsonParserService<T>
	{

		private readonly ILoggerService _loggerService;

		public JsonParserService(ILoggerService loggerService)
		{
			_loggerService = loggerService;
		}

		public T GetObjectFromJson(string json)
		{
			_loggerService.LogInfo(LoggerMessages.LoggerEndDeserializeMessage);
			return JsonConvert.DeserializeObject<T>(json);
		}

		public string GetJsonFromObject(T article)
		{
			return JsonConvert.SerializeObject(article);
		}
	}
}