﻿using Services.Interfaces;
using Services.Model;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
	public class FileManagerService : IFileManagerService
	{

		private readonly ILoggerService _loggerService;

		public FileManagerService(ILoggerService loggerService)
		{
			_loggerService = loggerService;
		}

		public string ReadFromFile(string path)
		{
			_loggerService.LogInfo(LoggerMessages.LoggerReadDataMessage);
			using (FileStream fstream = new FileStream(path, FileMode.Open))
			{
				byte[] array = new byte[fstream.Length];
				fstream.Read(array, 0, array.Length);
				return Encoding.Default.GetString(array);
			}
		}

		public async Task WriteToFileAsync(string path, string data)
		{
			_loggerService.LogInfo(LoggerMessages.LoggerWriteDataMessage);
			using (FileStream fstream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None, bufferSize: 4096, useAsync: true))
			{
				byte[] array = Encoding.Default.GetBytes(data);
				await fstream.WriteAsync(array, 0, array.Length);
			}
		}
	}
}