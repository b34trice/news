﻿using Data;
using Services.Interfaces;
using Services.Model;
using System.Collections.ObjectModel;

namespace Services
{
	public class MapperService : IMapperService
	{

		private readonly ILoggerService _loggerService;

		public MapperService(ILoggerService loggerService)
		{
			_loggerService = loggerService;
		}

		public ObservableCollection<Article> GetArticles(BaseArticleModel baseArticles)
		{

			ObservableCollection<Article> articles = new ObservableCollection<Article>();

			if (baseArticles == null)
			{
				return articles;
			}

			foreach (BaseArticleResults baseArticle in baseArticles.Results)
			{
				_loggerService.LogTrace(LoggerMessages.LoggerStartNewArticleMessage);
				Article article = new Article
				{
					Abstract = baseArticle.Abstract,
					Context = baseArticle.Section,
					Title = baseArticle.Title,
					Url = baseArticle.Url,
					Byline = baseArticle.Byline,
					PublishedDate = baseArticle.First_published_date
				};

				if (string.IsNullOrEmpty(baseArticle.Subsection))
				{
					article.Context += $"\\{baseArticle.Subsection}";
				}

				if (baseArticle.Multimedia != null && baseArticle.Multimedia.Count >= 3)
				{
					article.ArticleImage = GetArticleImage(baseArticle.Multimedia[3]);
				}
				else
				{
					article.ArticleImage = new ArticleImage();
				}

				articles.Add(article);

				_loggerService.LogTrace(LoggerMessages.LoggerEndArticleMessage);

			}
			_loggerService.LogInfo(LoggerMessages.LoggerEndMapperWorkMessage);
			return articles;
		}

		private ArticleImage GetArticleImage(BaseArticleImage baseArticleImage)
		{
			_loggerService.LogInfo(LoggerMessages.LoggerStartMappingImageMessage);
			ArticleImage articleImage = new ArticleImage
			{
				Url = baseArticleImage.Url,
				Height = baseArticleImage.Height,
				Width = baseArticleImage.Width
			};

			_loggerService.LogInfo(LoggerMessages.LoggerEndMappingImageMessage);
			return articleImage;
		}
	}
}