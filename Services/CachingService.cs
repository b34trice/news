﻿using Data;
using Services.Interfaces;
using Services.Model;
using System;
using System.Collections.ObjectModel;
using System.IO;
using Windows.Storage;

namespace Services
{
	public class CachingService : ICachingService
	{
		private readonly string _applicationCachePath = $"{ApplicationData.Current.LocalFolder.Path}/Cache.json";

		private readonly ILoggerService _loggerService;
		private readonly IFileManagerService _fileManagerService;
		private readonly IMapperService _mapperService;
		private readonly IJsonParserService<BaseArticlesStore> _jsonParserService;

		public CachingService(ILoggerService loggerService, IFileManagerService fileManagerService, IMapperService mapperService, IJsonParserService<BaseArticlesStore> jsonParserService)
		{
			_loggerService = loggerService;
			_fileManagerService = fileManagerService;
			_mapperService = mapperService;
			_jsonParserService = jsonParserService;
		}

		public ObservableCollection<Article> GetCache()
		{
			_loggerService.LogInfo(LoggerMessages.LoggerStartReadCacheMessage);
			if (!IsNeedLoadCashe())
			{
				return null;
			}
			BaseArticlesStore baseArticles = _jsonParserService.GetObjectFromJson(_fileManagerService.ReadFromFile(_applicationCachePath));
			ObservableCollection<Article> articles = _mapperService.GetArticles(baseArticles.baseArticle);
			_loggerService.LogInfo(LoggerMessages.LoogerEndReadCacheMessage);
			return articles;
		}

		public void WriteCache(BaseArticlesStore articles)
		{
			_loggerService.LogInfo(LoggerMessages.LoggerStartWriteCacheMessage);
			string jsonCache = _jsonParserService.GetJsonFromObject(articles);
			_fileManagerService.WriteToFileAsync(_applicationCachePath, jsonCache);
			_loggerService.LogInfo(LoggerMessages.LoggerEndWriteCacheMessage);
		}

		private bool IsNeedLoadCashe()
		{
			if (!File.Exists(_applicationCachePath))
			{
				_loggerService.LogInfo(LoggerMessages.LoggerCacheIsMissingMessage);
				WriteCache(null);
				return false;
			}

			DateTime writeCacheTime = new FileInfo(_applicationCachePath).LastWriteTime;
			TimeSpan timeSubstract = DateTime.Now.Subtract(writeCacheTime);

			if (timeSubstract.TotalMinutes > 3)
			{
				return false;
			}

			_loggerService.LogInfo(LoggerMessages.LoggerCacheIsActualMessage);
			return true;
		}
	}
}