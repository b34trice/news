﻿using NLog;
using Services.Interfaces;
using Windows.Storage;

namespace Services
{
	public class LoggerService : ILoggerService
	{
		private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

		public void Config()
		{
			NLog.Config.LoggingConfiguration config = new NLog.Config.LoggingConfiguration();

			NLog.Targets.FileTarget logfile = new NLog.Targets.FileTarget("logfile") { FileName = $"{ApplicationData.Current.LocalFolder.Path}/Logs.txt" };
			logfile.DeleteOldFileOnStartup = true;
			config.AddRule(LogLevel.Trace, LogLevel.Fatal, logfile);
			LogManager.Configuration = config;
		}

		public void LogDebug(string message)
		{
			_logger.Debug(message);
		}

		public void LogError(string message)
		{
			_logger.Error(message);
		}

		public void LogFatal(string message)
		{
			_logger.Fatal(message);
		}

		public void LogInfo(string message)
		{
			_logger.Info(message);
		}

		public void LogTrace(string message)
		{
			_logger.Trace(message);
		}

		public void LogWarning(string message)
		{
			_logger.Warn(message);
		}
	}
}