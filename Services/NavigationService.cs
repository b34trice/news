﻿using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Windows.UI.Xaml.Controls;

namespace Services
{
	public class NavigationService : INavigationService
	{
		private Frame _frame;

		private readonly IDictionary<string, Type> _pages = new Dictionary<string, Type>();

		public string CurrentPage()
		{
			return _pages.Single(i => i.Value == _frame.Content.GetType()).Key;
		}

		public NavigationService()
		{
			_frame = new Frame();
		}

		public void SetFrame(Frame frame)
		{
			_frame = frame;
		}

		public void GoBack()
		{
			if (_frame.CanGoBack)
			{
				_frame.GoBack();
			}
		}

		public void NavigateTo(string page)
		{
			NavigateTo(page, null);
		}

		public void NavigateTo(string page, object parameter)
		{
			_frame.Navigate(_pages[page], parameter);
		}

		public void SetPageConfig(string pageName, Type type)
		{
			if (!_pages.ContainsKey(pageName))
			{
				_pages.Add(pageName, type);
			}
		}
	}
}