﻿using Data;
using System.Collections.ObjectModel;

namespace Services.Interfaces
{
	public interface ICachingService
	{
		ObservableCollection<Article> GetCache();

		void WriteCache(BaseArticlesStore articles);
	}
}
