﻿using System;
using Windows.UI.Xaml.Controls;

namespace Services.Interfaces
{
	public interface INavigationService
	{
		void NavigateTo(string page);

		void NavigateTo(string page, object parameter);

		void SetFrame(Frame frame);

		void GoBack();

		void SetPageConfig(string pageNamemType, Type type);

	}
}