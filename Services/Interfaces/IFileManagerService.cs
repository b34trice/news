﻿using System.Threading.Tasks;

namespace Services.Interfaces
{
	public interface IFileManagerService
	{
		string ReadFromFile(string path);

		Task WriteToFileAsync(string path, string data);
	}
}
