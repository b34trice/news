﻿using Services.Model;

namespace Services.Interfaces
{
	public interface IAppSettingsService
	{
		AppSettings GetSettings();

		void SetSettings(AppSettings newSettings);
	}
}
