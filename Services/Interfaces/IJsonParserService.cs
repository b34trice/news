﻿namespace Services.Interfaces
{
	public interface IJsonParserService<T>
	{
		T GetObjectFromJson(string json);

		string GetJsonFromObject(T article);
	}
}
