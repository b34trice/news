﻿using Data;
using System.Collections.ObjectModel;

namespace Services.Interfaces
{
	public interface IMapperService
	{
		ObservableCollection<Article> GetArticles(BaseArticleModel baseArticle);
	}
}