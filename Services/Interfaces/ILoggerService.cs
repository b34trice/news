﻿namespace Services.Interfaces
{
	public interface ILoggerService
	{
		void LogInfo(string message);

		void LogWarning(string message);

		void LogError(string message);

		void LogFatal(string message);

		void LogDebug(string message);

		void LogTrace(string message);

		void Config();

	}
}
