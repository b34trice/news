﻿using System.Collections.ObjectModel;

namespace Services.Interfaces
{
	public interface IApiService
	{
		ObservableCollection<Article> GetArticles(int articlesCounter);
	}
}
