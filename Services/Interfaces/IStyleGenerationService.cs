﻿using Services.Model;
using Windows.UI.Xaml;

namespace Services.Interfaces
{
	public interface IStyleGenerationService
	{
		Style GetStyle(AppSettings settings);
	}
}