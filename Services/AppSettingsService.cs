﻿using Services.Interfaces;
using Services.Model;
using System.IO;
using Windows.Storage;
using Windows.UI.Xaml;

namespace Services
{
	public class AppSettingsService : IAppSettingsService
	{
		private readonly string _filePath = $"{ApplicationData.Current.LocalFolder.Path}/Settings.json";

		private static AppSettings _settings;

		private readonly ILoggerService _loggerService;
		private readonly IFileManagerService _fileManagerService;
		private readonly IJsonParserService<AppSettings> _jsonParserService;

		public AppSettingsService(ILoggerService loggerService, IFileManagerService fileManagerService,IJsonParserService<AppSettings> jsonParserService)
		{
			_loggerService = loggerService;
			_fileManagerService = fileManagerService;
			_jsonParserService = jsonParserService;
		}

		public AppSettings GetSettings()
		{
			_loggerService.LogInfo(LoggerMessages.LoggerGetSettingsMessage);
			if (_settings == null)
			{
				_settings = _jsonParserService.GetObjectFromJson(GetJsonFromFile());
				return _settings;
			}
			else
			{
				return _settings;
			}
		}

		public void SetSettings(AppSettings newSettings)
		{
			_settings = newSettings;
			_fileManagerService.WriteToFileAsync(_filePath, _jsonParserService.GetJsonFromObject(_settings));
		}

		private string GetJsonFromFile()
		{
			if (File.Exists(_filePath))
			{
				return _fileManagerService.ReadFromFile(_filePath);
			}
			else
			{
				_loggerService.LogInfo(LoggerMessages.LoggerMissingFileMessage);

				AppSettings settings = new AppSettings(15, ElementTheme.Dark);
				string json = _jsonParserService.GetJsonFromObject(settings);
				_fileManagerService.WriteToFileAsync(_filePath, json);

				return json;
			}
		}
	}
}