﻿using System;

namespace Services
{
	internal class UriBuilderService
	{
		private const string _baseUrl = "https://api.nytimes.com/svc/news/v3/content/all/all.json";
		private const string _offset = "offset=";
		private const string _limit = "limit=5";
		private const string _apiKey = "api-key=userOsj1a7wYz1yG7HOPIVIdQ1T1J05y";

		public Uri GetUri(int articlesCounter)
		{
			string url = $"{_baseUrl}?{_limit}&{_offset}{articlesCounter}&{_apiKey}";
			return new Uri(url);
		}
	}
}